package com.example.alejandro.spinner;

import android.opengl.EGLExt;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etNum1, etNum2;
    private TextView tvResultado;
    private Spinner spOpcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNum1 = (EditText)findViewById(R.id.txtNum1);
        etNum2 = (EditText)findViewById(R.id.txtNum2);
        tvResultado = (TextView)findViewById(R.id.lblResultado);
        spOpcion = (Spinner)findViewById(R.id.spOpciones);

        String []opciones = {"SUMAR","RESTAR","MULTIPLICAR","DIVIDIR"};

        //ArrayAdapter<String> adaptador = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,opciones);
        ArrayAdapter<String> adaptador = new ArrayAdapter<>(this, R.layout.spinner_item,opciones);

        spOpcion.setAdapter(adaptador);
    }

    public void calcular(View vista)
    {
        int operacion;
        String resultado;
        String num1 = etNum1.getText().toString();
        String num2 = etNum2.getText().toString();

        int EntNum1 = Integer.parseInt(num1);
        int EntNum2 = Integer.parseInt(num2);

        String seleccion = spOpcion.getSelectedItem().toString();

        if (seleccion.equals("SUMAR"))
        {
            operacion = EntNum1 + EntNum2;
            resultado = String.valueOf(operacion);
            tvResultado.setText(resultado);
        }else if (seleccion.equals("RESTAR"))
        {
            operacion = EntNum1 - EntNum2;
            resultado = String.valueOf(operacion);
            tvResultado.setText(resultado);
        }else if (seleccion.equals("MULTIPLICAR"))
        {
            operacion = EntNum1 * EntNum2;
            resultado = String.valueOf(operacion);
            tvResultado.setText(resultado);
        }else if (seleccion.equals("DIVIDIR"))
        {
            if (EntNum2 != 0)
            {
                operacion = EntNum1 / EntNum2;
                resultado = String.valueOf(operacion);
                tvResultado.setText(resultado);
            }else
            {
                Toast.makeText(this,"No se puede dividir entre 0", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
